﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Exam_23_12_2019
{
    public class WorkUnit
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public DateTime Date { get; set; }
        public string Result { get; set; }
    }
}
