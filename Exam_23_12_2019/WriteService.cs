﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Exam_23_12_2019
{
    public class WriteService
    {
        public async void WriteToFile(string fileName, WorkUnit unit)
        {
            using (StreamWriter stream = File.AppendText(fileName))
            {
                await stream.WriteLineAsync($"{unit.Date} : {unit.Result}");
            }
        }
        public async void WriteToDatabase(WorkUnit unit)
        {
            using (var context = new AppContext())
            {
                context.Results.Add(unit);
                await context.SaveChangesAsync();
            }
        }
    }
}
