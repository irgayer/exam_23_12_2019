﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Exam_23_12_2019
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const string DEFAULT_FILENAME = "results.txt";

        private WriteService writeService;

        public MainWindow()
        {
            InitializeComponent();
            writeService = new WriteService();
        }

        private async void StartButtonClick(object sender, RoutedEventArgs e)
        {
            startButton.IsEnabled = false;
            StringBuilder builder = new StringBuilder(string.Empty);

            for (int i = 0; i < 1000; i++)
            {
                await Task.Run(() =>
                {
                    builder.Append($"{i.ToString()} ");
                });
            }

            WorkUnit unit = new WorkUnit()
            {
                Date = DateTime.Now,
                Result = builder.ToString()
            };

            MessageBox.Show(unit.Result);
            writeService.WriteToDatabase(unit);
            writeService.WriteToFile(DEFAULT_FILENAME, unit);

            //using (var context = new AppContext())
            //{
            //    MessageBox.Show(context.Results.Count().ToString());
            //}

            startButton.IsEnabled = true;
        }

    }
}
