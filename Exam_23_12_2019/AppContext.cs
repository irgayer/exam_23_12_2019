﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Entity;

namespace Exam_23_12_2019
{
    public class AppContext : DbContext
    {
        public AppContext() : base("DbConnection")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<AppContext>());
        }
     

        public DbSet<WorkUnit> Results { get; set; }
    }
}
